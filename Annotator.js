/*

The annotator add to each Fabric object :

  * _old : the previous values of the attributes in order to compute changes
  * _id : the id of the object for the Annotator

Beware, the 'changes' array may not be sorted by times if the merge of
two concurrents editing session is done.

*/

function Annotator() {
        this.parse_options();
        this.highlight_opacity = '8'; // Hexa
        this.teacher_opacity = 'A'; // Hexa
        this.precision = 1000;
        this.ignore = {}; // The Fabric attributes to not save
        var ignore = 'version styles visible shadow strokeDashOffset strokeLineJoin strokeMiterLimit flipX flipY globalCompositeOperation skewX skewY charSpacing splitByGrapheme linethrough paintFirst clipTo transformMatrix'.split(' ');
        for (var i in ignore)
                this.ignore[ignore[i]] = true;
        this.defaults = {
                'originX': 'left',
                'originY': 'top',
                "strokeWidth": 1,
                "scaleX": 1,
                "scaleY": 1,
                "angle": 0,
                "opacity": 1,
                "backgroundColor": "",
                "fillRule": "nonzero",
                "text": "Tapez votre texte.",
                "fontSize": 40,
                "fontWeight": "normal",
                "fontFamily": "Times New Roman",
                "fontStyle": "normal",
                "lineHeight": 1.16,
                "underline": false,
                "overline": false,
                "textAlign": "left",
                "textBackgroundColor": "",
                "minWidth": 20,
                "stroke": null,
                "strokeDashArray": null
        }
        this.annotator = document.getElementById('annotator');
        this.tags = document.getElementById('tags');
        this.menu = document.getElementById('menu');
        this.message = document.getElementById('message');
        this.undoed = [];
        this.changes = [];
        this.last_save = 0;
        this.animate = document.getElementById('animate');
        this.history = document.getElementById('history');
        this.init_animate();

        this.canvas = new fabric.Canvas('annotator');
        this.canvas.on('object:modified', this.onmodified.bind(this));
        this.canvas.on('object:added', this.onadded.bind(this));
        this.canvas.on('object:removed', this.onremoved.bind(this));
        this.canvas.on('mouse:up', this.onclick.bind(this));
        this.canvas.on('selection:created', this.onselect.bind(this));

        addEventListener('keyup', this.onkeypress.bind(this), false);
        window.onresize = this.onresize.bind(this);
        window.onhashchange = this.onhashchange.bind(this);

        this.shortcuts = {};
        this.create_widths();
        this.create_sizes();
        this.create_actions();
        this.create_modes();
        this.create_colors();

        this.saves = document.getElementById('save');
        if (this.options.save)
                this.b_save = this.add_button(this.saves, '<span>💾</span>',
                        this.save.bind(this), 's|Ctrl+s]', undefined, 'save');
        this.b_grade = this.add_button(this.saves, '<span></span>',
                this.explain_grade.bind(this), undefined, undefined, 'grade');

        document.body.scrollTop = 0;
        this.onhashchange(true);
        this.debug();
}

Annotator.prototype.parse_options = function () {
        this.options = {
                'debug': 0,
                'teacher': 0,
                'colors': '#000,#F00,#0F0,#00F', // ,#0FF,#F0F,#FF0',
                'color': '#000',
                'widths': '1,4,8,12',
                'width': 1,
                'sizes': '18,24,32,48',
                'size': 24,
                'image': 'demo.png',
                'annotations': 'empty.txt',
                'save': '?',
                'modes': undefined,
                'me': 'anonymous',
                'mode': 'Text'
        };
        var options = location.hash.substr(1).split('&');
        for (var i in options) {
                var option = options[i].split('=');
                this.options[option[0]] = isNaN(option[1])
                        ? decodeURIComponent(option[1])
                        : Number(option[1]);
        }
        if (!this.options.modes)
                this.options.modes = this.options.teacher
                        ? 'Edit,Text,Draw,Highlight,-1,+1,-0.5,+0.5,-0.25,+0.25,😟,😊'
                        : 'Edit,Text,Draw,Highlight';
        if (this.options.save)
                this.key = this.options.save.split("?")[0]; // Remove the ticket

};

Annotator.prototype.load_background = function () {
        this.image = new Image()
        this.image.src = this.options.image;
        this.ratio = Math.sqrt(2); // A4
        this.image.onload = (function () {
                this.ratio = this.image.height / this.image.width;
                window.onresize();
        }).bind(this);
        this.annotator.style.backgroundImage = 'url(' + this.options.image + ')';
        this.options.image = this.options.image.replace(/[0-9]+\?/,
                (new Date()).getTime() + '?') // Disable caching
};

Annotator.prototype.onhashchange = function (first) {
        if (first !== true) {
                this.save_localstorage();
                if (this.changes.length != this.load_index)
                        if (confirm('💾'))
                                this.save(true);
                        else
                                delete localStorage[this.key];
        }
        this.parse_options();
        this.reset();
        this.load_background();
        this.request(this.options.annotations, this.load.bind(this));
};

Annotator.prototype.save_localstorage = function () {
        if (!this.options.save)
                return;
        this.last_save = (new Date()).getTime();
        localStorage[this.key] = JSON.stringify(
                this.changes.slice(
                        this.real_load_index,
                        this.changes.length
                ));
};

Annotator.prototype.show_message = function (message) {
        this.message.innerHTML = message;
        if (message == '⚠')
                this.message.className += ' display error';
        else
                this.message.className += ' display';
};

Annotator.prototype.hide_message = function () {
        setTimeout(function () { this.message.className = ''; }, 100);
};

Annotator.prototype.message_visible = function () {
        return this.message.className !== '';
};

Annotator.prototype.save = function (hashchange) {
        if (this.options.save == '?') {
                this.show_message('💾<br>OK!');
                this.hide_message();
                return;
        }
        var url = this.options.save;
        var xmlhttp = new XMLHttpRequest();
        var formData = new FormData();
        this.canvas.discardActiveObject(); // Terminate pending text editing
        this.save_localstorage();
        this.options.save = undefined;
        var annotator = this;
        var key = annotator.key; // Because key change on hashchange
        formData.append("content", localStorage[this.key]);
        this.load_index_save = this.load_index;
        this.load_index = this.changes.length; // no more undoable
        this.update_undo_redo();
        document.body.className += ' read_only';
        xmlhttp.open("POST", url, true);
        xmlhttp.onloadend = function () {
                if (this.responseText.toString().indexOf('OK!') != -1) {
                        annotator.show_message('💾<br>OK!');
                        delete localStorage[key];
                        if (!hashchange) {
                                annotator.options.save = url;
                                annotator.request(annotator.options.annotations,
                                        annotator.load.bind(annotator));
                        }
                        return;
                }
                else
                        alert(this.responseText);
        };
        xmlhttp.onerror = function () {
                annotator.show_message('⚠');
                if (!hashchange) {
                        annotator.load_index = this.load_index_save;
                        annotator.update_undo_redo(); // Allow to retry
                        annotator.options.save = url;
                }
        };
        xmlhttp.send(formData);
};

Annotator.prototype.explain_grade = function () {
        console.log('grade');
};

Annotator.prototype.create_modes = function () {
        this.modes = document.getElementById('modes');
        this.palette_modes = this.options.modes.split(',');
        var modes = {
                'Edit': ['➚', 'd'], 'Text': ['<b>T</b>', 't'],
                'Draw': ['✍', 'c'], 'Highlight': ['🖍', 'f'],
                '+1': ['+1', '+'], '-1': ['-1', '-']
        };
        this.b_modes = {};
        for (var i in this.palette_modes) {
                let m = this.palette_modes[i];
                var title_shortcut = modes[m] || [m, undefined];
                var mode = title_shortcut[0];
                this.b_modes[m] = this.add_button(this.modes,
                        '<span'
                        + (m > 0 ? ' class="point_plus"' : '')
                        + (m < 0 ? ' class="point_minus"' : '')
                        + '>'
                        + mode.replace('0.5', '½').replace('0.25', '¼')
                        + '</span>'
                        + (m.length <= 3 && isNaN(m) ? '<i></i>' : '')
                        ,
                        (function () { this.set_mode(m); }).bind(this),
                        title_shortcut[1],
                        m);
        }
        this.set_mode(this.options.mode);
};

Annotator.prototype.create_colors = function () {
        this.colors = document.getElementById('colors');
        this.palette_colors = {
                '#000': 'N', '#F00': 'R', '#0F0': 'V', '#00F': 'B',
                '#FF0': 'J', '#0FF': 'C', '#F0F': 'M',

                "#0A0": 'V', "#0CC": 'C', "#C0C": 'M', "#CC0": 'J'
        };
        this.b_colors = [];
        for (var color in this.palette_colors)
                if (this.options.colors.indexOf(color) != -1) {
                        let c = color;
                        this.b_colors.push(this.add_button(
                                this.colors,
                                '<span style="color:' + c + '">⬤</span>',
                                (function () { this.set_color(c); }).bind(this),
                                this.palette_colors[color],
                                color
                        ));
                }
        this.set_color(this.options.color);
};

Annotator.prototype.create_widths = function () {
        this.widths = document.getElementById('widths');
        this.palette_widths = this.options.widths.split(',');
        this.b_widths = [];
        for (var i in this.palette_widths) {
                let width = this.palette_widths[i];
                this.b_widths.push(this.add_button(
                        this.widths,
                        '<span style="transform: scale(' + width / 10 + ',' + width / 10
                        + ')">⬤</span>',
                        (function () { this.set_width(width); }).bind(this),
                        (Number(i) + 1).toString()
                ));
        }
        this.set_width(this.options.width);
};

Annotator.prototype.create_sizes = function () {
        this.sizes = document.getElementById('sizes');
        this.palette_sizes = { '18': 'p', '24': 'm', '32': 'g', '48': 'e' };
        this.b_sizes = [];
        for (var s in this.palette_sizes)
                if (this.options.sizes.indexOf(s) != -1) {
                        let size = s;
                        this.b_sizes.push(this.add_button(
                                this.sizes,
                                '<span style="transform: scale(' + size / 20 + ',' + size / 20
                                + ')">T</span>',
                                (function () { this.set_size(size); }).bind(this),
                                this.palette_sizes[s],
                                size
                        ));
                }
        this.set_size(this.options.size);
};

Annotator.prototype.create_actions = function () {
        this.actions = document.getElementById('actions');
        this.b_undo = this.add_button(this.actions, '<span>⟲</span><i></i>',
                this.action_undo, 'z|Ctrl+z');
        this.b_redo = this.add_button(this.actions, '<span>⟳</span><i></i>',
                this.action_redo, 'y|Ctrl+y|Ctrl+Z');
        this.b_help = this.add_button(this.actions, '<span><b>?</b></span>',
                this.action_help, '?');
        this.b_delete = this.add_button(this.actions, '<span>🗑</span><i></i>',
                this.action_delete, 'Suppr|Delete');
};

Annotator.prototype.load = function (txt) {
        document.body.className += ' read_only';
        this.reset();
        if (txt.length) {
                txt = txt.trim().split('\n');
                for (var i in txt)
                        this.changes.push(JSON.parse(txt[i]));
        }
        this.real_load_index = this.load_index = this.changes.length;
        if (localStorage[this.key]) {
                var changes = JSON.parse(localStorage[this.key]);
                for (var i in changes)
                        this.changes.push(changes[i]);
        }
        this.jump_to(this.changes.length);
        this.undoed = [];
        this.onresize();
        this.hide_message();
        this.init_animate();
};

Annotator.prototype.request = function (url, callback, retry_time) {
        this.load_index = 0;
        if (!this.message_visible())
                this.show_message("⌛");
        var annotator = this;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.the_url = url;
        xmlhttp.the_callback = callback;
        xmlhttp.open("GET", url, true);
        xmlhttp.setRequestHeader("If-Modified-Since", "Thu, 1 Jan 1970 00:00:00 GMT");
        xmlhttp.setRequestHeader("Cache-Control", "no-cache");
        xmlhttp.overrideMimeType("text/plain; charset=ascii");
        xmlhttp.retry_time = retry_time || 1000;
        xmlhttp.onload = function () {
                if (retry_time)
                        annotator.load_background();
                xmlhttp.the_callback(xmlhttp.responseText);
        };
        xmlhttp.onerror = function () {
                setTimeout(function () {
                        annotator.request(xmlhttp.the_url, xmlhttp.the_callback,
                                xmlhttp.retry_time * 2);
                }, xmlhttp.retry_time);
        }
        xmlhttp.send();
};

Annotator.prototype.add_button = function (parent, content, method, shortcut,
        index, cls) {
        var e = document.createElement('BUTTON');
        if (cls)
                e.className = cls;
        if (method) {
                method = method.bind(this);
                e.onmousedown = function (event) {
                        method();
                        event.cancelBubble = true;
                        event.preventDefault(true);
                        event.stopPropagation(true);
                };
                if (shortcut) {
                        if (this.shortcuts[shortcut])
                                bug;
                        content = '<tt>' + shortcut.split('|')[0]
                                + '</tt>' + content;
                        this.shortcuts[shortcut] = method;
                }
        }
        else
                e.disabled = true;
        e.innerHTML = content;
        e.my_index = index;
        parent.appendChild(e);
        return e;
};

Annotator.prototype.set_width = function (width) {
        for (var i in this.b_widths)
                this.b_widths[i].className = this.palette_widths[i] == width ? 'selected' : '';
        this.canvas.freeDrawingBrush.width = width;
        this.stroke_width = width;
        this.update_attribute('strokeWidth', width);
};

Annotator.prototype.set_size = function (size) {
        for (var i in this.b_sizes)
                this.b_sizes[i].className = this.b_sizes[i].my_index == size ? 'selected' : '';
        this.text_size = size;
        this.update_attribute('fontSize', size);
};

Annotator.prototype.set_color = function (color) {
        for (var i in this.b_colors)
                this.b_colors[i].className = this.b_colors[i].my_index == color ? 'selected' : '';
        this.canvas.freeDrawingBrush.color = color
                + (this.mode == 'Highlight' ? this.highlight_opacity : '');
        this.color = color;
        if (this.options.teacher) {
                this.update_attribute('backgroundColor', color + this.teacher_opacity);
                this.update_attribute('fill', '#FFF');
        }
        else
                this.update_attribute('fill', color);
        // var style = 'filter: grayscale(100%) drop-shadow(2px 2px 0px ' + color + ')' ;
        // this.b_modes['Text'].lastChild.style = style ;
        // this.b_modes['Draw'].lastChild.style = style ;
        // this.b_modes['Highlight'].lastChild.style = style ;

        this.update_attribute('stroke', color);
};

Annotator.prototype.update_attribute = function (name, value) {
        var objects = this.canvas.getActiveObjects();
        for (var i in objects) {
                var obj = objects[i];
                if (obj.type == 'textbox') {
                        if (name != 'fill' && name != 'fontSize'
                                && name != 'backgroundColor')
                                continue;
                }
                else if (obj.type == 'path') {
                        if (name != 'strokeWidth' && name != 'stroke')
                                continue;
                        if (name == 'stroke' && obj.stroke.length == 5)
                                value += this.highlight_opacity;
                }
                obj.set(name, value);
                this.onmodified({ target: obj });
        }
        this.canvas.renderAll();
};

Annotator.prototype.set_mode = function (mode) {
        this.mode = mode;
        this.points = isNaN(mode) ? 0 : Number(mode);
        this.click_mode = this.points || mode.length <= 3;
        if (this.click_mode) {
                this.canvas.discardActiveObject();
                this.canvas.renderAll();
                this.disable_next_click = false;
        }
        this.canvas.selection = !this.click_mode;
        this.canvas.isDrawingMode = mode == 'Draw' || mode == 'Highlight';
        this.canvas.freeDrawingBrush.width = (mode == 'Highlight' ? 20 : this.stroke_width);
        this.canvas.freeDrawingBrush.color = this.color + (mode == 'Highlight' ? '8' : '');
        this.canvas.freeDrawingBrush.decimate = 5;

        this.canvas.defaultCursor = mode == 'Text' ? 'text' : 'default';
        for (var i in this.b_modes)
                this.b_modes[i].className = mode == i ? 'selected' : '';
        for (var i in this.b_widths)
                this.b_widths[i].disabled = mode != 'Draw' && mode != 'Edit';
        for (var i in this.b_sizes)
                this.b_sizes[i].disabled = mode != 'Text' && mode != 'Edit';
        for (var i in this.b_colors)
                this.b_colors[i].disabled = this.click_mode;
        this.update_selectability();
};

Annotator.prototype.action_undo = function () {
        var range = this.get_range(this.changes.length);
        if (range[0] == this.load_index)
                return;
        var undoed = this.changes.splice(range[0] - 1, this.changes.length - range[0] + 1);
        for (var i in undoed)
                this.undoed.push(undoed[i]);
        this.jump_to(range[0] - 1);
};

Annotator.prototype.action_redo = function () {
        if (this.undoed.length == 0)
                return;
        var change = this.undoed.pop();
        this.changes.push(change);
        var time = change[1];
        while (this.undoed.length && this.undoed[this.undoed.length - 1][1] == time)
                this.changes.push(this.undoed.pop());
        this.jump_to(this.changes.length);
};

Annotator.prototype.update_undo_redo = function () {
        this.b_undo.disabled = this.changes.length == this.load_index;
        if (this.b_save)
                this.b_save.disabled = this.b_undo.disabled;

        if (this.b_undo.disabled)
                this.b_undo.lastChild.innerHTML = '';
        else
                this.b_undo.lastChild.innerHTML = this.changes.length - this.load_index;

        this.b_redo.disabled = this.undoed.length == 0;
        this.b_redo.lastChild.innerHTML = this.undoed.length || '';
};

Annotator.prototype.action_delete = function () {
        var objects = this.canvas.getActiveObjects();
        if (objects.length == 0)
                return;
        for (var i in objects)
                this.canvas.remove(objects[i]);
        this.canvas.discardActiveObject();
        this.b_delete.disabled = true;
};

Annotator.prototype.action_help = function () {
        this.display_help = this.b_help.className === '';
        this.b_help.className = this.display_help ? 'selected' : '';
        this.onresize();
};

Annotator.prototype.tags_recreate = function () {
        function D(n) { return (n < 10 ? '0' : '') + n; };
        this.tags.innerHTML = '';
        if (!this.display_help)
                return;
        for (var i in this.changes) {
                var o = this.objects[this.changes[i][2]];
                if (o)
                        o._last_change = i;
        }
        for (var i in this.objects) {
                var o = this.objects[i];
                if (!o || o._last_change === undefined)
                        continue;
                var e = document.createElement('DIV');
                var d = new Date();
                d.setTime(1000 * this.changes[o._last_change][1]);
                var qui = this.changes[o._last_change][4];
                e.innerHTML = D(d.getDate()) + '/' + D(d.getMonth() + 1) + '/' + d.getFullYear()
                        + '<br>' + D(d.getHours()) + ':' + D(d.getMinutes()) + ':' + D(d.getSeconds())
                        + (qui ? '<br>' + qui : ''); // + '<br>' + o._user;
                e.style.left = this.annotator.parentNode.offsetLeft + o.left * this.zoom + 'px';
                e.style.top = o.top * this.zoom + 'px';
                e.style.transform = ' translate(-50%, -50%) rotate(' + this.objects[i].angle + 'deg) translate(50%,50%)';
                this.tags.appendChild(e);
        }
};

Annotator.prototype.onkeypress = function (event) {
        if (event.target.tagName != 'BODY')
                return;
        var key = (event.ctrlKey ? 'Ctrl+' : '') + event.key;
        for (var i in this.shortcuts) {
                var re = RegExp('^(' + i.replace(/([+?])/g, '\\$1') + ')$');
                if (key.match(re)) {
                        this.shortcuts[i]();
                        break;
                }
        }
};

Annotator.prototype.timestamp = function () {
        // Timestamps have a one second resolution
        var d = new Date();
        return Math.floor(d.getTime() / 1000);
};

Annotator.prototype.get_range = function (n) {
        // Find the changes made the same timestamp
        if (n == 0)
                return [0, 0];
        n--;
        var first = n, last = n;
        var t = this.changes[n][1];
        while (first && this.changes[first - 1][1] == t)
                first--;
        while (last < this.changes.length - 1 && this.changes[last + 1][1] == t)
                last++;
        return [first + 1, last + 1];
};

Annotator.prototype.highlight_history = function (n) {
        if (!this.options.debug)
                return;
        var h = this.history.innerHTML.replace(/<\/?b>/g, '');
        if (n) {
                var t = this.changes[n - 1][1];
                h = h.replace(RegExp('(.*' + t + '.*)', 'g'), '<b>$1</b>');
        }
        this.history.innerHTML = h;
};

Annotator.prototype.animate_jump_to = function (_event, n) {
        if (n === undefined)
                n = this.changes.length;
        this.jump_to(this.get_range(n)[1]);
};

Annotator.prototype.animate_update = function () {
        if (this.current_n === undefined || !this.time_travel)
                return;
        var range = this.get_range(this.current_n);
        this.animate.firstChild.style.top = range[0] * this.window_height / (this.changes.length + 1) + 'px';
        this.animate.firstChild.style.height = (range[1] - range[0] + 1)
                * this.window_height / (this.changes.length + 1) + 'px';
        if (this.current_n) {
                var d = new Date();
                d.setTime(this.changes[this.current_n - 1][1] * 1000);
                function F(x) {
                        x = x.toString();
                        return ('0' + x).substr(x.length - 1);
                }
                this.animate.firstChild.innerHTML =
                        F(d.getDate())
                        + '/' + F(d.getMonth() + 1)
                        + '/' + d.getFullYear()
                        + ' <br>' + F(d.getHours()) // Do not remove space
                        + ':' + F(d.getMinutes())
                        + ':' + F(d.getSeconds());
        }
        else {
                this.animate.firstChild.innerHTML = '∅';
        }
        this.highlight_history(range[1]);
};

Annotator.prototype.animate_move = function (event) {
        // this is the HTML 'animate' element (onmousemove)
        var n = this.changes.length + 1;
        n *= event.y / this.window_height;
        n = Math.floor(n);
        if (n > this.changes.length)
                n = this.changes.length;
        this.animate_jump_to(event, n);
}


Annotator.prototype.init_animate = function () {
        if (!this.options.teacher)
                return;
        this.time_travel = true;
        var A = this;
        this.animate.onmousemove = function (event) { A.animate_move(event); };
        this.animate.onmouseout = function (event) { A.animate_jump_to(event); };
};

Annotator.prototype.debug = function () {
        if (!this.options.debug)
                return;
        var t = [];
        for (var i in this.changes) {
                if (i == this.load_index)
                        t.push('⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔ ⛔');
                t.push(JSON.stringify(this.changes[i]));
        }
        this.history.innerHTML = t.join('\n');
        this.highlight_history(this.current_n);
        this.init_animate();
};

Annotator.prototype.reset = function () {
        this.canvas.clear();
        this.changes = []; // The timestamped history of changes
        this.id = 1; // The id of the next object to be created
        this.objects = {}; // id ==> Fabric
        this.current_n = undefined;
};

Annotator.prototype.update_selectability = function () {
        // Update selectable state
        for (var i in this.objects) {
                this.objects[i].selectable = !this.click_mode
                        && this.options.me == this.objects[i]._user;
                if (this.objects[i].selectable)
                        delete this.objects[i].hoverCursor;
                else
                        this.objects[i].hoverCursor = 'default';
        }
};

Annotator.prototype.jump_to = function (n) {
        // Replay the history from start until 'n'
        var full_changes = this.changes;
        var changes = full_changes.slice(0, n);
        this.loading = true;
        this.reset();
        this.current_n = n;
        for (var i in changes) {
                var change = changes[i];
                if (change[0] == 'new') {
                        if (change[2] == 1000000)
                                this.id = 1000000;
                        change[3]._user = change[4];
                        fabric.util.enlivenObjects([change[3]],
                                (function (objects) {
                                        this.canvas.add(objects[0]);
                                }).bind(this));
                        continue;
                }
                var obj = this.objects[change[2]];
                if (!obj) {
                        console.log(this.objects);
                        console.log(this.options.save);
                        alert('bug:' + change);
                        delete localStorage[this.key];
                        location.reload();
                        return;
                }
                switch (change[0]) {
                        case 'chg':
                                obj.set(change[3]);
                                obj.setCoords();
                                break;
                        case 'del':
                                this.canvas.remove(obj);
                                break;
                }
        }
        this.update_selectability();
        this.loading = false;
        if (this.id < 1000000)
                this.id = 1000000;
        this.changes = full_changes;
        this.b_delete.disabled = true;
        this.update_interface();
};

Annotator.prototype.get_clean_dict = function (target) {
        var obj = this.canvas._toObject(target, 'toObject');
        if (obj['path']) {
                var p = obj['path'];
                for (var i in p)
                        for (var j = 1; j < p[i].length; j++)
                                p[i][j] = Number(p[i][j].toFixed(1));
        }
        if (obj['text'] == this.defaults['text'])
                obj['text'] = '';
        var clean = {};
        for (var k in obj)
                if (!this.ignore[k] && k.substr(0, 1) != '_'
                        && obj[k] !== this.defaults[k])
                        clean[k] = obj[k];
        return clean;
}

Annotator.prototype.update_grade = function () {
        var s = 0;
        var nb = {};
        for (var i in this.objects) {
                if (!this.objects[i].backgroundColor)
                        continue; // Not teacher
                var v = this.objects[i].text;
                var n = Number(v);
                if (isNaN(n)) {
                        if (this.b_modes[v])
                                nb[v] = (nb[v] || 0) + 1;
                }
                else
                        s += n;
        }
        this.b_grade.lastChild.innerHTML = s;
        for (var i in this.b_modes)
                if (this.b_modes[i].lastChild.tagName == 'I')
                        this.b_modes[i].lastChild.innerHTML = nb[i] || 0;
};

Annotator.prototype.onmodified = function (opt) {
        var target = opt.target;
        if (!target._old) {
                for (var i in target._objects)
                        this.onmodified({ target: target._objects[i] });
                return;
        }
        var obj = this.get_clean_dict(target);
        var changes = {};
        var change = false;
        for (var k in obj)
                if (JSON.stringify(obj[k]) != JSON.stringify(target._old[k])) {
                        target._old[k] = changes[k] = obj[k];
                        change = true;
                }
        if (change) {
                if (obj.left > this.precision
                        || obj.top > this.precision * this.ratio
                        || obj.left + obj.width * obj.scaleX < 0
                        || obj.top + obj.height * obj.scaleY < 0
                ) {
                        this.canvas.remove(target);
                        return;
                }
                this.changes.push(['chg', this.timestamp(), target._id, changes]);
                if (!this.loading) {
                        this.current_n = this.changes.length;
                        this.update_interface();
                }
        }
        this.tags_recreate();
};
Annotator.prototype.update_interface = function () {
        if (this.loading)
                return;
        this.animate_update();
        if (this.update_pending)
                return;
        this.update_pending = true;
        setTimeout((function () {
                this.update_pending = false;
                this.update_undo_redo();
                this.update_grade();
                this.tags_recreate();
                this.debug();
        }).bind(this), 100);
};

Annotator.prototype.onadded = function (opt) {
        var target = opt.target;
        target._id = this.id++;
        target._old = {};
        this.objects[target._id] = target;
        var obj = this.get_clean_dict(target);
        for (var k in obj)
                target._old[k] = obj[k];
        if (!this.loading) {
                target._user = this.options.me;
                this.changes.push(['new', this.timestamp(), target._id, obj]);
                this.current_n = this.changes.length;
                this.undoed = [];
                this.update_interface();
        }
        target._user = target._user || this.options.me;
};

Annotator.prototype.onremoved = function (opt) {
        this.changes.push(['del', this.timestamp(), opt.target._id, '']);
        delete this.objects[opt.target._id];
        if (!this.loading) {
                this.current_n = this.changes.length;
                this.undoed = [];
                this.update_interface();
        }
};

Annotator.prototype.onselect = function (_opt) {
        this.disable_next_click = true;
};

Annotator.prototype.onclick = function (opt) {
        if ((new Date()).getTime() - this.last_save > 1000)
                this.save_localstorage();

        var actives = this.canvas.getActiveObjects();
        if (actives.length) {
                this.b_delete.disabled = false;
                this.b_delete.lastChild.innerHTML = actives.length;
                return;
        }
        this.b_delete.disabled = true;
        if (this.disable_next_click) {
                this.disable_next_click = false;
                return;
        }

        var t;
        var attrs = {
                left: opt.absolutePointer.x,
                top: opt.absolutePointer.y,
                fill: this.options.teacher ? "#FFF" : this.color,
                fontSize: this.text_size
        };
        if (attrs.left < 0 || attrs.left > this.precision
                || attrs.top < 0 || attrs.top > this.precision * this.ratio)
                return;
        if (this.mode == 'Text') {
                t = this.defaults['text'];
                attrs.width = (this.canvas.width - opt.pointer.x) / this.zoom;
                if (this.options.teacher)
                        attrs.backgroundColor = this.color + this.teacher_opacity;
        }
        if (this.click_mode) {
                t = this.mode;
                attrs.backgroundColor = (this.points < 0 ? '#C00' :
                        (this.points > 0 ? '#080' : '#FFF'))
                        + this.teacher_opacity;
                if (!this.points) {
                        attrs.fontFamily = 'emoji';
                        attrs.fill = '#000';
                }
                attrs.fontSize = 40;
                // attrs.selectable = false ; // Allow to change text
        }
        if (!t)
                return;
        t = new fabric.Textbox(t, attrs);
        this.canvas.add(t);
        if (this.click_mode)
                this.disable_next_click = false;
        else {
                this.canvas.setActiveObject(t);
                t.selectAll();
                t.enterEditing();
        }
};

Annotator.prototype.onresize = function () {
        this.window_width = window.innerWidth;
        this.window_height = window.innerHeight;
        document.body.className =
                (this.window_width > this.window_height ? ' double' : '')
                + (this.display_help ? ' help' : '')
                + (this.options.debug ? ' debug' : '')
                + (this.options.teacher ? ' teacher' : '')
                + (this.time_travel ? ' time_travel' : '')
                + (this.options.save ? '' : ' read_only')
                ;
        this.width = this.menu.offsetLeft - this.animate.offsetWidth - 2;
        this.canvas.setWidth(this.width);
        this.canvas.setHeight(this.width * this.ratio);
        this.zoom = this.width / this.precision;
        this.canvas.setZoom(this.zoom);
        this.menu.style.position = this.menu.offsetHeight < this.window_height
                ? 'fixed' : 'absolute';
        this.update_interface();
};

function start_annotator() {
        try { var a = new Annotator(); console.log(a); }
        catch { setTimeout(100, start_annotator); }
}

start_annotator();
