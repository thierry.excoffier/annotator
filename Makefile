demo:demo.png fabric.js
	xdg-open "file://$$(pwd)/Annotator.html#annotations=demo.txt&save=https://&me=p1234567"

fabric.js:
	wget 'http://cdnjs.cloudflare.com/ajax/libs/fabric.js/3.6.3/fabric.min.js'
	mv fabric.min.js $@

demo.png:demo.svg
	inkscape --export-png=$@ $?

install:demo.png
	-cp -a --update [a-zA-Z]* $$HOME/public_html/ANNOTATOR
	-chmod 644 $$HOME/public_html/ANNOTATOR/[a-zA-Z]*

clean:
	-rm demo.png fabric.js

recup_from_tomuss:
	cp ~/T/FILES/Annotator.js Annotator.js
	cp ~/T/FILES/Annotator.css Annotator.css
	cp ~/T/FILES/Annotator.html Annotator.html
